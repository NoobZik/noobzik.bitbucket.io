
#include <stdlib.h>
#include <stdio.h>

/*-------- TD1 -----------------------------------*/

/*------------------------------------------------*/
/*-------- Exercice 1, Fonction Factoriel---------*/
/*------------------------------------------------*/

int factorielle (int n) {
    if(n == 0){
        return 1;
    }
    else{
        return factorielle(n-1)*n;
    }
}

/*
int main(){
    int x = 4;
    printf("Le factorielle de %d est %d", x, factorielle(x));
}
*/

/*------------------------------------------------*/
/*--------Exercice 2 -----------------------------*/
/*------------------------------------------------*/

int addition(x,y){
    if(y == 0){
        return x;
    }
    else{
        return addition(x+1,y-1);
    }
}