<!DOCTYPE html>



<html>
	<head>
		<meta charset="utf-8">
		<meta content="utf-8" http-equiv="encoding">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="stylesheet" href="/vendor/font-awesome/css/font-awesome.min.css">

		<link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!--link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,300,600,700,800,400' rel='stylesheet' type='text/css'--> 

		<link href="/vendor/flexslider/flexslider.css" rel="stylesheet">
		<link href="/vendor/prettyphoto/css/prettyPhoto.css" rel="stylesheet">
		<link href="/css/style.css" rel="stylesheet">
		
		<title>Rakib Hernandez | Cours / TD : Elements Informatique</title>

	</head>
	<body>
		<!-- Header -->
		<?php include_once("../../analyticstracking.php") ?>
		<header_1>
			<!--img src="img/background/tablet/01_Background_image_1.jpg" class="hidden-xs big-header">
			<img src="img/background/mobile/01_Background_image_1.jpg" class="visible-xs big-header"-->	
					<div id="menu">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
				  		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-content">
						    <span class="sr-only">Toggle navigation</span>
						    <span class="icon-bar"></span>
						    <span class="icon-bar"></span>
						    <span class="icon-bar"></span>
					  	</button>
						  <a class="navbar-brand" href="/index.html">
					  		<img src="/img/other/03_Logo.png"> <span>    Rakib Sheikh</span>
						  </a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="menu-content">
				  		<ul class="nav navbar-nav navbar-right">
						    <li><a href="#">Home</a></li>
						    <li><a href="../../cours.php">Partiel / CC</a></li>
						    <li><a href="#cours">Cours</a></li>
						    <li><a href="#td">Travaux Dirigé</a></li>
						    <li><a href="#tp">Travaux Pratique</a></li>
						    <!--li><a href="#portfolio">Portfolio</a></li-->
						    <li><a href="#contact">Contact</a></li>
				  		</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		</div>
		</header_1>
		

		<!-- Project -->
		<section>
			<div class="container">
				<div class="row">
					<h2>Cours TD/TP - Elements Informatique -Institut Galilée</h2>
				</div>
				
				<blockquote>
					<p>Vous y trouverez ici tout les cours et les travaux dirigé de l'année passé</p>
				</blockquote>
			</div>
		</section>
		
		<section id="cours">
			<div class="container">
				<!-- COURS-->
					<h3><b>Cours diaporama passé en Amphi</b></h3>
					
						
							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"> <li> Cours 1 : Architecture des ordinateurs, Mini-assembleur </li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/cours1-papier.pdf"> Cours 1</a></li></div></div>

							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"> <li> Cours 2 : Elements de système d'exploitation </li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/cours2-papier.pdf"> Cours 2</a></li></div></div>

							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"> <li> Cours 3 : Programmation Structuré langage C et instruction du contrôle if </li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/cours3-papier.pdf"> Cours 3</a></li></div></div>

							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"> <li> Cours 4 : Expression booléennes. Fonction d'entrée/sortie. Principe de la compilation </li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/cours4-papier.pdf"> Cours 4</a></li></div></div>

							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"> <li> Cours 5 : Type de données et représentations </li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/cours5-papier.pdf"> Cours 5</a></li></div></div>

							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"> <li> Cours 6 : Boucle While - Algorithmes élémentaires </li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/cours6-papier.pdf"> Cours 6</a></li></div></div>

							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"> <li> Cours 7 : Tableaux </li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/cours7-papier.pdf"> Cours 7</a></li></div></div>
						
			</div>


		</section>
				<!-- TRAVAUX DIRIGE -->
		<section id="td">
			<div class="container">
					<h3><b>Travaux Dirigés TD</b></h3>
					
						
							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"><li>  Travaux dirigée 1 : Affectation</li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/td1.pdf">TD 1</a></div></div>
							
							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"><li>  Travaux dirigée 2 : Programmation mini-assembleur</li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/td2.pdf"> TD 2</a></div></div>
							
							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"><li>  Travaux dirigée 3 : Exercices introductifs sur variable impératives, la structure de contrôle if et l'itération for</li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/td3.pdf"> TD 3</a></div></div>
							
							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"><li>  Travaux dirigée 4 : structure de contrôle if et structure de contrôle for - Approfondissement</li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/td4.pdf"> TD 4</a></div></div>
							
							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"><li>  Travaux dirigée 5 : type booléen en C; Lecture de données au clavier et types.</li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/td5.pdf"> TD 5</a></div></div>
							
							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"><li>  Travaux dirigée 6 : structures de contrôle for et while, type réel</li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/td6.pdf"> TD 6</a></div></div>
							
							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"><li>  Travaux dirigée 7 : les tableaux de variables</li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/td7.pdf"> TD 7</a></div></div>
							
							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"><li>  Travaux dirigée 8 : écriture et appel de fonctions et procédures</li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/td8.pdf"> TD 8</a></div></div>


						
			</div>
		</section>
				<section id="tp">
			<div class="container">
					<h3><b>Travaux Pratique TP</b></h3>
					
						
							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"><li>  Travaux pratique 1 : Premiers pas Linux - commandes Linux</li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/tp1.pdf"> TP 1</a></div></div>
							
							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"><li>  Travaux pratique 2 : Programmation avec Blockly</li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/tp2.pdf"> TP 2</a></div></div>
							
							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"><li>  Travaux pratique 3 : premiers pas en langage C, structure de contrôle if et intération for</li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/tp3.pdf"> TP 3</a></div></div>
							
							<div class="col-md-8 col-md-4 col-sm-10 col-xs-8"><li>  Travaux pratique 4 : structure de contrôle while; évaluation d'expressions booléennes; type de données</li></div>
							<div class="row"><div class="col-xs-offset-1 pull-right"><a class="btn btn-primary btn-xs view-pdf" href="td/tp4.pdf"> TP 4</a></div></div>


						
			</div>
		</section>

		<!-- Testimonial -->
		<!--section id="testimonial" class="clearfix" id="">
		   <div class="container">
		      <div class="testimonial">
		         <div class="row">
		         	<div class="flexslider">
		         		<ul class="slides">
				            <li>
				            	<div class="col-sm-10 col-sm-offset-1">
					               <p class="voice">
					                  "Proin tristique vitae est quis lacinia duis cursus ligula nec."
					               </p>
					               <p class="position">
					                  Thomas Brown - Art director at DREAM WORLD.ltd
					               </p>
				           		</div>
				            </li>
				            <li>
				            	<div class="col-sm-10  col-sm-offset-1">
					               <p class="voice">
					                  "Proin 2 tristique vitae est quis lacinia duis cursus ligula nec."
					               </p>
					               <p class="position">
					                  Thomas 2 Brown - Art director at DREAM WORLD.ltd
					               </p>
				               </div>
				            </li>
			            </ul>
		            </div>
		         </div>
		      </div>
		   </div>
		</section-->

		<!-- contact footer -->
<section id="contact">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 widget w-info">
						<p class="title">
							let's socialize
						</p-->
						<ul class="clearfix">
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-facebook-square"></i>
								</a>
							</li>
							<!--li class="col-xs-2">
								<a href="#">
									<i class="fa fa-twitter-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-google-plus-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-pinterest-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-linkedin-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-github-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-rss"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-dribbble"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-vimeo-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-youtube-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-flickr"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-instagram"></i>
								</a>
							</li-->
						</ul--!>
						<p>Sevran - France</p>
						<p>Phone: (033) 1 43 xx xx xx</p>
						<p>Email: <a href="mailto:rakib.hernandez@gmail.com" target="_top">rakib.hernandez-at-gmail.com</a></p>
						<img src="/img/footer/openshift.png" class="img-responsive">
					</div>
					<div class="col-md-8 widget w-contact">
						<p class="title">
							send a pigeon (Desactivé, en cours de développement)
						</p>
						<form class="clearfix">
						<fieldset disabled>
							<input name="name" class="form-control" id="disabledTextInput" type="textbox" placeholder="Votre nom:">
							<input name="email" class="form-control" id="disabledTextInput" type="textbox" placeholder="Votre mail:">
							<input name="subject" class="form-control" id="disabledTextInput" type="textbox" placeholder="Sujet:">
							<textarea name="message" class="form-control" id="disabledTextInput" placeholder="Message:"></textarea>
							<input type="submit" class="form-control" id="disabledTextInput" name="send" disabled="disabled" value="Envoyer">
						</fieldset>
						</form>
					</div>
					
				</div>
			</div>
		</section>

		<div id="overlay">
				<img src="/img/other/loading.gif">
		</div>

		<!-- footer -->


		<script type="text/javascript" src="/vendor/jquery2.js"></script>
		<script type="text/javascript" src="/vendor/moderniz.js"></script>
		<script type="text/javascript" src="/vendor/jquery.easing.1.3.js"></script>
		 <!-- <script type="text/javascript" src="vendor/jquery.stellar.min.js"></script> -->
		<script type="text/javascript" src="/vendor/waypoints.min.js"></script>
		<script type="text/javascript" src="/vendor/bootstrap/js/bootstrap.min.js"></script>
		
		<script type="text/javascript" src="/vendor/jquery.easypiechart.min.js"></script>
		<script type="text/javascript" src="/vendor/flexslider/jquery.flexslider.js"></script>
		<script type="text/javascript" src="/vendor/jquery-scrollto.js"></script>
		<script type="text/javascript" src="/vendor/jquery.quicksand.js"></script>
		<script type="text/javascript" src="/vendor/placeholders.min.js"></script>
		<script type="text/javascript" src="/vendor/prettyphoto/js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="/vendor/imagesloaded.pkgd.min.js"></script>
		
		<script type="text/javascript" src="/js/main.js"></script>

	</body>
</html> 
