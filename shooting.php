<!DOCTYPE html>

<html>
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1.0">
		
		<link rel="stylesheet" href="/vendor/font-awesome/css/font-awesome.min.css">

		<link href="/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
		<!--link href='http://fonts.googleapis.com/css?family=Open+Sans:300italic,300,600,700,800,400' rel='stylesheet' type='text/css'--> 

		<link href="/vendor/flexslider/flexslider.css" rel="stylesheet">
		<link href="/vendor/prettyphoto/css/prettyPhoto.css" rel="stylesheet">
		<link href="/css/style_old.css" rel="stylesheet">
		
		<title>Rakib Hernandez Sheikh | Photographie</title>

	</head>
	<body>
	<?php include_once("analyticstracking.php") ?>
		<!-- Header -->
		<div id="menu">
			<nav class="navbar navbar-default" role="navigation">
				<div class="container">
					<!-- Brand and toggle get grouped for better mobile display -->
					<div class="navbar-header">
				  		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu-content">
						    <span class="sr-only">Toggle navigation</span>
						    <span class="icon-bar"></span>
						    <span class="icon-bar"></span>
						    <span class="icon-bar"></span>
					  	</button>
						  <a class="navbar-brand" href="index.html">
					  		<img src="/img/other/03_Logo.png"> <span>    Rakib Sheikh</span>
						  </a>
					</div>

					<!-- Collect the nav links, forms, and other content for toggling -->
					<div class="collapse navbar-collapse" id="menu-content">
				  		<ul class="nav navbar-nav navbar-right">
						    <li><a href="#">Home</a></li>
						    <li><a href="#service">Services</a></li>
						    <li><a href="#portfolio">Portfolio</a></li>
						    <li><a href="#contact">Contact</a></li>
				  		</ul>
					</div><!-- /.navbar-collapse -->
				</div><!-- /.container-fluid -->
			</nav>
		</div>
		<header>
			<img src="/img/background/tablet/01_Background_image_1.jpg" class="hidden-xs big-header">
			<img src="/img/background/mobile/01_Background_image_1.jpg" class="visible-xs big-header">	
			<div class="container">
				<h2 class="main-headline">Let's become a model</h2>
			</div>
		</header>
		
		<!-- Service section -->
		<section id="service" class="feature">
			<div class="container">
				<div class="row">
					<h2 class="sec-title">Services</h2>
				</div>
				<div class="row center-block">
					<!--div class="col-sm-3 item">
						<div class="i-wrapper">
							<i class="fa fa-puzzle-piece fa-fw">
			            	</i>
			            </div>
						<h4>Graphic design</h4>
						<div class="sep"></div>
						<p>
							Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore.
						</p>
					</div-->
					<div class="col-sm-3 item">
                    
						<div class="i-wrapper">
							<i class="fa fa-camera fa-fw"></i>
						</div>
						<h4></br>Photographie</h4>
						<div class="sep"></div>
						<p>
							J'ai commencé à faire de la photographie à partir de 2014, je travaille actuellement en Freelance.</br>
							Je travaille avec un Sony DSC-HX300
						</p>
					</div>
					<div class="col-sm-3 item">
						<div class="i-wrapper">
							<i class="fa fa-video-camera fa-fw"></i>
						</div>
						<h4></br>Vidéaste</h4>
						<div class="sep"></div>
						<p>
							La vidéo graphie est un atout essentiel en ce qui concerne la promotion d'une marque, ceci est ma philosophie.</br>
							Pour le moment, je fais des tournages de vine en attendant le matériel requis pour le professionnel.
						</p>
					</div>
					<div class="col-sm-3 item">
						<div class="i-wrapper">
							<i class="fa fa-cloud-upload fa-fw"></i>
						</div>
						<h4></br>Webmaster</h4>
						<div class="sep"></div>
						<p>
							Un site internet doit avant tout être à la hauteur de la clientèle et faire face a la concurrence.</br>Rendre un site web attractif est essentiel pour attirer la clientèle.</br>
							<a href="/data.php">Découvrez mes compétences ici</a>
						</p>
					</div>
                
				</div>
			</div>
		</section>
		<!-- End of sevice section -->

		<!-- Emphasize section -->
	<!-- 	<section id="emphasize" class="center-back">
			<img src="img/background/tablet/02_Background_image_2.jpg" alt="" class="hidden-xs">
			<img src="img/background/mobile/02_Background_image_2.jpg" alt="" class="visible-xs">
			<div class="center-text">
				<p>
					My design is </br>
					about sexy
				</p>
			</div>
		</section> -->
		<!-- End emphasize section -->

		<!-- Parallax-->


		<!-- Experience-->
		

		

		<!-- Project -->
		<section id="portfolio" class="items-wrap">
		
		<?php include("scot.html"); ?>



		</section>

		<!-- Testimonial -->


		<!-- contact footer -->
		<section id="contact">
			<div class="container">
				<div class="row">
					<div class="col-sm-4 widget w-info">
						<p class="title">
							let's socialize
						</p-->
						<ul class="clearfix">
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-facebook-square"></i>
								</a>
							</li>
							<!--li class="col-xs-2">
								<a href="#">
									<i class="fa fa-twitter-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-google-plus-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-pinterest-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-linkedin-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-github-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-rss"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-dribbble"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-vimeo-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-youtube-square"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-flickr"></i>
								</a>
							</li>
							<li class="col-xs-2">
								<a href="#">
									<i class="fa fa-instagram"></i>
								</a>
							</li-->
						</ul--!>
						<p>Sevran - France</p>
						<p>Phone: (033) 1 43 xx xx xx</p>
						<p>Email: <a href="mailto:rakib.hernandez@gmail.com" target="_top">rakib.hernandez-at-gmail.com</a></p>
						<img src="/img/footer/openshift.png" class="img-responsive">
					</div>
					<div class="col-md-8 widget w-contact">
						<p class="title">
							send a pigeon (Desactivé, en cours de développement)
						</p>
						<form class="clearfix">
						<fieldset disabled>
							<input name="name" class="form-control" id="disabledTextInput" type="textbox" placeholder="Votre nom:">
							<input name="email" class="form-control" id="disabledTextInput" type="textbox" placeholder="Votre mail:">
							<input name="subject" class="form-control" id="disabledTextInput" type="textbox" placeholder="Sujet:">
							<textarea name="message" class="form-control" id="disabledTextInput" placeholder="Message:"></textarea>
							<input type="submit" class="form-control" id="disabledTextInput" name="send" disabled="disabled" value="Envoyer">
						</fieldset>
						</form>
					</div>
					
				</div>
			</div>
		</section>

		<div id="overlay">
				<img src="/img/other/loading.gif">
		</div>

		<!-- footer -->

		<!-- The Bootstrap Image Gallery lightbox, should be a child element of the document body -->
<div id="blueimp-gallery" class="blueimp-gallery blueimp-gallery-controls">
    <!-- The container for the modal slides -->
    <div class="slides"></div>
    <!-- Controls for the borderless lightbox -->
    <h3 class="title">title</h3>
    <a class="prev">‹</a>
    <a class="next">›</a>
    <a class="close">×</a>
    <!-- The modal dialog, which will be used to wrap the lightbox content -->    
</div>

		<script type="text/javascript" src="/vendor/jquery2.js"></script>
		<script type="text/javascript" src="/vendor/moderniz.js"></script>
		<script type="text/javascript" src="vendor/jquery.easing.1.3.js"></script>
		 <!-- <script type="text/javascript" src="vendor/jquery.stellar.min.js"></script> -->
		<script type="text/javascript" src="/vendor/waypoints.min.js"></script>
		<script type="text/javascript" src="/vendor/bootstrap/js/bootstrap.min.js"></script>
		
		<script type="text/javascript" src="/vendor/jquery.easypiechart.min.js"></script>
		<script type="text/javascript" src="/vendor/flexslider/jquery.flexslider.js"></script>
		<script type="text/javascript" src="/vendor/jquery-scrollto.js"></script>
		<script type="text/javascript" src="/vendor/jquery.quicksand.js"></script>
		<script type="text/javascript" src="/vendor/placeholders.min.js"></script>
		<script type="text/javascript" src="/vendor/prettyphoto/js/jquery.prettyPhoto.js"></script>
		<script type="text/javascript" src="/vendor/imagesloaded.pkgd.min.js"></script>
		
		<script type="text/javascript" src="/js/main.js"></script>


	</body>
</html> 
